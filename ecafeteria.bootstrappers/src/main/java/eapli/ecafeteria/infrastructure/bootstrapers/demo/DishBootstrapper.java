/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrapers.demo;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

import eapli.ecafeteria.dishmanagement.application.RegisterDishController;
import eapli.ecafeteria.dishmanagement.application.viadto.RegisterDishViaDTOController;
import eapli.ecafeteria.dishmanagement.domain.Allergen;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.ecafeteria.dishmanagement.repositories.AllergenRepository;
import eapli.ecafeteria.dishmanagement.repositories.DishTypeRepository;
import eapli.ecafeteria.infrastructure.bootstrapers.TestDataConstants;
import eapli.ecafeteria.infrastructure.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 * @author mcn
 */
public class DishBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DishBootstrapper.class);

    private final DishTypeRepository dishTypeRepo = PersistenceContext.repositories().dishTypes();
    private final AllergenRepository allergenRepo = PersistenceContext.repositories().allergens();

    // dependency injection - when constructing the object one must inject the dependencies to
    // infrastructure objects it needs. this should be handled by a DI/IoC container like Spring
    // Framework
    private final RegisterDishController controllerViaDomain = new RegisterDishController(
            AuthzRegistry.authorizationService(), PersistenceContext.repositories().dishes(),
            PersistenceContext.repositories().dishTypes(), PersistenceContext.repositories().allergens());

    private final RegisterDishViaDTOController controllerViaDTO = new RegisterDishViaDTOController();

    private DishType getDishType(final String acronym) {
        return dishTypeRepo.ofIdentity(acronym).orElseThrow(IllegalStateException::new);
    }

    private Allergen getAllergen(final String name) {
        return allergenRepo.ofIdentity(name).orElseThrow(IllegalStateException::new);
    }

    @Override
    public boolean execute() {
        final var vegie = getDishType(TestDataConstants.DISH_TYPE_VEGIE);
        final var fish = getDishType(TestDataConstants.DISH_TYPE_FISH);
        final var meat = getDishType(TestDataConstants.DISH_TYPE_MEAT);

        final var gluten = getAllergen(TestDataConstants.ALLERGEN_GLUTEN);
        final var peixe = getAllergen(TestDataConstants.ALLERGEN_PEIXES);
        final var crustaceos = getAllergen(TestDataConstants.ALLERGEN_CRUSTACEOS);

        final Set<Allergen> allergens1 = new HashSet<>();
        allergens1.add(gluten);
        allergens1.add(peixe);
        final Set<Allergen> glutenAllergens = new HashSet<>();
        glutenAllergens.add(gluten);
        final Set<Allergen> crustaceosAllergens = new HashSet<>();
        crustaceosAllergens.add(crustaceos);
        final Set<Allergen> noAllergens = new HashSet<>();

        register(vegie, TestDataConstants.DISH_NAME_TOFU_GRELHADO, 140, 1, 2.99, null);
        register(vegie, TestDataConstants.DISH_NAME_LENTILHAS_SALTEADAS, 180, 1, 2.85, noAllergens);
        register(fish, TestDataConstants.DISH_NAME_BACALHAU_A_BRAZ, 250, 2, 3.99, allergens1);
        register(fish, TestDataConstants.DISH_NAME_LAGOSTA_SUADA, 230, 2, 24.99, crustaceosAllergens);
        register(meat, TestDataConstants.DISH_NAME_PICANHA, 375, 2, 4.99, crustaceosAllergens);
        register(meat, TestDataConstants.DISH_NAME_COSTELETA_A_SALSICHEIRO, 475, 2, 3.99, glutenAllergens);

        // one additional dish (using a DTO instead of domain object)
        registerViaDTO(vegie, "Ananaz grelhado", 200, 2, 0.99);

        return true;
    }

    private Optional<Dish> register(final DishType dishType, final String description,
            final int cal, final int salt,
            final double price, final Set<Allergen> allergens) {
        try {
            LOGGER.debug("{} ( {} )", description, dishType);

            return Optional.of(
                    controllerViaDomain.registerDish(dishType, description, cal, salt, price, allergens));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    description,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }

    private Optional<DishDTO> registerViaDTO(final DishType dishType, final String description,
            final int cal, final int salt, final double price) {
        try {
            LOGGER.debug("{} ( {} )", description, dishType);

            final var dish = new DishDTO(dishType.identity(), dishType.description(), description, cal, salt, price,
                    "EUR", true);
            return Optional.of(controllerViaDTO.registerDish(dish));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    description,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
