/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.backoffice;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.dishmanagement.application.viadto.ChangeDishViaDTOController;
import eapli.ecafeteria.dishmanagement.dto.ChangeDishNutricionalInfoDTO;
import eapli.ecafeteria.dishmanagement.dto.ChangeDishPriceDTO;
import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.ecafeteria.infrastructure.bootstrapers.TestDataConstants;
import eapli.framework.actions.Action;
import eapli.framework.validations.Invariants;

/**
 *
 * @author Paulo Gandra de Sousa 2021.05.18
 *
 */
public class DishManagementViaDTOSmokeTester implements Action {
	private static final Logger LOGGER = LogManager.getLogger(DishManagementViaDTOSmokeTester.class);

	private final ChangeDishViaDTOController changeDishController = new ChangeDishViaDTOController();

	@Override
	public boolean execute() {
		testFindAllAndChangeDish();
		testSearchAndChangeDish();
		testSearchAndUpdateDish();

		// nothing else to do
		return true;
	}

	/**
	 * This smoke test simulates the user searching for a dish.
	 */
	private void testSearchAndUpdateDish() {
		// the user searches for the desired dish
		final DishDTO picanha = changeDishController.searchDish(TestDataConstants.DISH_NAME_PICANHA)
				.orElseThrow(IllegalStateException::new);

		// the UI will present the dish and allow the user to enter the changed data
		// (e.g., in a
		// data grid)
		picanha.setPrice(25);
		picanha.setSalt(5);
		picanha.setCalories(20);
		picanha.setActive(false);

		// the UI calls the controller
		final var updated = changeDishController.updateDish(picanha);

		Invariants.ensure(updated.getCalories() == 20 && updated.getSalt() == 5 && updated.getPrice() == 25
				&& !updated.isActive());

		LOGGER.info("»»» Updated dish via DTO {}", updated.getName());
	}

	/**
	 * This smoke test simulates the user selecting the entry in a dropdown UI.
	 */
	private void testFindAllAndChangeDish() {
		final Iterable<DishDTO> l = changeDishController.allDishes();
		Invariants.nonNull(l);

		final var dish = l.iterator().next();

		doChangeDishAndLog(dish);
	}

	/**
	 * This smoke test simulates the user searching for a dish.
	 */
	private void testSearchAndChangeDish() {
		final Optional<DishDTO> changed = changeDishController.searchDish(TestDataConstants.DISH_NAME_PICANHA)
				.map(this::doChangeDishAndLog);
		if (!changed.isPresent()) {
			LOGGER.info("»»» The dish {} does not exist", TestDataConstants.DISH_NAME_PICANHA);
		}
	}

	private DishDTO doChangeDishAndLog(final DishDTO dish) {
		final var changedDish = doChangeDishPrice(dish);

		return doChangeDishNutricionalInfo(changedDish);
	}

	private DishDTO doChangeDishNutricionalInfo(final DishDTO dish) {
		// change nutricional info
		final var newInfo = new ChangeDishNutricionalInfoDTO(dish.getName(), 10, 2);
		final var changed = changeDishController.changeDishNutricionalInfo(newInfo);
		Invariants.ensure(changed.getCalories() == 10 && changed.getSalt() == 2);

		LOGGER.info("»»» Changed nutricional info of {} via DTO", dish.getName());

		return changed;
	}

	private DishDTO doChangeDishPrice(final DishDTO dish) {
		// change price
		final var newPrice = new ChangeDishPriceDTO(dish.getName(), 31, dish.getCurrency());

		final var changed = changeDishController.changeDishPrice(newPrice);
		Invariants.ensure(changed.getPrice() == 31);

		LOGGER.info("»»» Changed price of {} via DTO", dish.getName());
		return changed;
	}
}
