/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.backoffice;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.dishmanagement.application.ActivateDeactivateDishTypeController;
import eapli.ecafeteria.dishmanagement.application.ChangeDishTypeController;
import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.ecafeteria.infrastructure.bootstrapers.TestDataConstants;
import eapli.ecafeteria.infrastructure.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.validations.Invariants;

/**
 * @author Paulo Gandra de Sousa 2021.05.18
 */
public class DishTypeManagementSmokeTester implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DishTypeManagementSmokeTester.class);

    // dependency injection - when constructing the object one must inject the dependencies to
    // infrastructure objects it needs. this should be handled by a DI/IoC container like Spring
    // Framework
    private final ChangeDishTypeController changeDishTypeController = new ChangeDishTypeController(
            AuthzRegistry.authorizationService(), PersistenceContext.repositories().dishTypes());
    private final ActivateDeactivateDishTypeController activateDishTypeController = new ActivateDeactivateDishTypeController(
            AuthzRegistry.authorizationService(), PersistenceContext.repositories().dishTypes());
    private final DishTypeCRUDSmokeTester crudTester = new DishTypeCRUDSmokeTester();

    @Override
    public boolean execute() {
        testDishTypeCRUD();
        testActivateDeactivateDishType();
        testChangeDishType();
        testChangeMeatDishType();

        // nothing else to do
        return true;
    }

    private void testDishTypeCRUD() {
        crudTester.testDishTypeCRUD();
    }

    /**
     * This smoke test just changes the first dish type it gets from the controller simulating the
     * user has selected one from a dropbox or some other UI widget.
     */
    private void testChangeDishType() {
        final Iterable<DishType> l = changeDishTypeController.dishTypes();
        Invariants.nonNull(l);

        final DishType dt = l.iterator().next();

        doChangeDishTypeAndLog(dt);
    }

    /**
     * This smoke test changes one specific dish type simulating the user has searched for dish
     * types by their acronym.
     */
    private void testChangeMeatDishType() {
        final Optional<DishType> searched = changeDishTypeController
                .searchDishType(TestDataConstants.DISH_TYPE_MEAT);
        final Optional<DishType> changed = searched.map(this::doChangeDishTypeAndLog);
        if (!changed.isPresent()) {
            LOGGER.info("»»» The dish type {} does not exist", TestDataConstants.DISH_TYPE_MEAT);
        }
    }

    private DishType doChangeDishTypeAndLog(final DishType dt) {
        final String newDescription = dt.description() + " (edited)";
        final var changedDishType = changeDishTypeController.changeDishType(dt,
                newDescription);
        Invariants.ensure(dt.description().equals(newDescription));

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("»»» Changed description of {} to {}", dt.identity(),
                    changedDishType.description());
        }

        return changedDishType;
    }

    private void testActivateDeactivateDishType() {
        final Iterable<DishType> l = activateDishTypeController.allDishTypes();
        Invariants.nonNull(l);

        DishType dish = l.iterator().next();

        final boolean oldStatus = dish.isActive();

        dish = activateDishTypeController.changeDishTypeState(dish);
        Invariants.ensure(dish.isActive() == !oldStatus);

        LOGGER.info("»»» Changed status of {} to {}", dish, dish.isActive());
    }
}
