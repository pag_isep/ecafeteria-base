/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.backoffice;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.integration.dishes.import_.application.ImportDishesController;
import eapli.framework.actions.Action;

/**
 *
 * @author Paulo Gandra de Sousa 30/04/2024
 *
 */
public class DishImportThruPluginSmokeTester implements Action {
	private static final Logger LOGGER = LogManager.getLogger(DishImportThruPluginSmokeTester.class);

	private final ImportDishesController importDishesController = new ImportDishesController();

	@Override
	public boolean execute() {
		// import one file in standard format
		testImportFrom("bootstrap1.dish");
		// import one file in alternate format
		testImportFrom("bootstrap2.dishx");

		// nothing else to do
		return true;
	}

	private void testImportFrom(final String file) {
		try {
			final var r = importDishesController.importDishes(file);
			outputImportedContent(file, r);
		} catch (final IOException e) {
			LOGGER.error("Error while importing dishes from {}", file, e);
		}
	}

	private void outputImportedContent(final String filename, final Iterable<Dish> dishes) {
		// output the imported content
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("-- IMPORT DISHES FROM {} USING A PLUGIN --", filename);

			for (final var d : dishes) {
				LOGGER.info(d.identity());
			}
			LOGGER.info("-- END IMPORT --");
		}
	}
}
