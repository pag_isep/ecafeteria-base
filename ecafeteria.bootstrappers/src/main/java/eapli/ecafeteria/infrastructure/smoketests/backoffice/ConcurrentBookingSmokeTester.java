/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.backoffice;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.cafeteriausermanagement.domain.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.MecanographicNumber;
import eapli.ecafeteria.cafeteriausermanagement.repositories.CafeteriaUserRepository;
import eapli.ecafeteria.infrastructure.bootstrapers.TestDataConstants;
import eapli.ecafeteria.infrastructure.persistence.PersistenceContext;
import eapli.ecafeteria.mealbooking.application.BookAMealForADayController;
import eapli.ecafeteria.mealbooking.application.BookAMealForADayControllerImpl;
import eapli.ecafeteria.mealbooking.domain.BookingToken;
import eapli.ecafeteria.mealmanagement.repositories.MealRepository;
import eapli.framework.actions.Action;
import eapli.framework.actions.TimedActions;

/**
 * Simulate a lot of concurrent users generating card movements and see how the custom generator
 * handles it.
 *
 * @author Paulo Gandra Sousa 19/05/2023
 */
public class ConcurrentBookingSmokeTester implements Action {
    private static final Logger LOGGER = LogManager.getLogger(ConcurrentBookingSmokeTester.class);

    private static final AtomicInteger COUNT_OF_ERRORS = new AtomicInteger();
    private static final AtomicInteger COUNT_OF_SUCESSFUL = new AtomicInteger();

    /**
     * The repository is hold on a class member variable. so when the run method
     * ends, there is nothing holding the thread from ending and being cleanup by
     * the JVM.
     *
     * @author Paulo Gandra Sousa 11/06/2021
     */
    private static class ConcurrentBookingThread extends Thread {
        private final BookAMealForADayController controller = new BookAMealForADayControllerImpl();
        private final MealRepository mealRepo = PersistenceContext.repositories().meals();
        private final CafeteriaUserRepository userRepo = PersistenceContext.repositories().cafeteriaUsers();

        @Override
        public void run() {
            LOGGER.info("Thread {} booking", Thread.currentThread().getName());
            try {

                final var meal = mealRepo
                        .findByDayAndType(TestDataConstants.DATE_TO_BOOK, TestDataConstants.MEAL_TYPE_TO_BOOK)
                        .iterator().next();

                final var number = eapli.framework.math.util.Math.heads() ? TestDataConstants.USER_TEST1
                        : TestDataConstants.USER_TEST2;
                final CafeteriaUser booker = userRepo
                        .ofIdentity(MecanographicNumber.valueOf(number))
                        .orElseThrow(IllegalStateException::new);
                final BookingToken token = controller.bookMeal(meal, booker);

                LOGGER.debug("Thread {} booked: {}", Thread.currentThread().getName(), token);
                COUNT_OF_SUCESSFUL.incrementAndGet();
            } catch (final Exception e) {
                LOGGER.error("Possibly the connection pool is exausted...", e);
                COUNT_OF_ERRORS.incrementAndGet();
            }
        }
    }

    @Override
    public boolean execute() {
        final var NTHREADS = 100;

        // helper debug - SHOULD NOT BE USED IN PRODUCTION CODE!!!
        int initialThreadCount = 0;
        if (LOGGER.isDebugEnabled()) {
            initialThreadCount = Thread.activeCount();
            LOGGER.debug("Starting thread tester - initial thread count: {}", initialThreadCount);
        }

        // create threads
        for (var i = 0; i < NTHREADS; i++) {
            LOGGER.info("Starting threads - class variables");
            new ConcurrentBookingThread().start();
        }
        LOGGER.info("Started {} threads", NTHREADS);

        // Let's wait a while and check
        while (NTHREADS > COUNT_OF_ERRORS.get() + COUNT_OF_SUCESSFUL.get()) {
            TimedActions.delay(500);
        }
        LOGGER.info("In the end we had {} sucessful calls and {} errors", COUNT_OF_SUCESSFUL.get(),
                COUNT_OF_ERRORS.get());

        // helper debug - SHOULD NOT BE USED IN PRODUCTION CODE!!!
        if (LOGGER.isDebugEnabled()) {
            final int finalThreadCount = Thread.activeCount();
            LOGGER.debug("Ending thread tester - final thread count: {} (initially were {})", finalThreadCount,
                    initialThreadCount);
            final Thread[] t = new Thread[finalThreadCount];
            final int n = Thread.enumerate(t);
            for (var i = 0; i < n; i++) {
                LOGGER.debug("T {}: {}", t[i].getId(), t[i].getName());
            }
        }

        return true;
    }

}
