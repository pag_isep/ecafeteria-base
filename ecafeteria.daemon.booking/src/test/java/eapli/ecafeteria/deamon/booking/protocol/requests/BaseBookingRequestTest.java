/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Optional;

import eapli.framework.csv.util.CsvLineMarshaler;
import eapli.framework.infrastructure.authz.application.Authenticator;
import eapli.framework.infrastructure.authz.application.UserSession;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;

/**
 * @author Paulo Gandra de Sousa 09/06/2020
 *
 */
public abstract class BaseBookingRequestTest {

	protected Authenticator mockAuthenticator() {
		return (username, rawPassword, requiredRoles) -> {
			final SystemUserBuilder userBuilder = new SystemUserBuilder(new NilPasswordPolicy(),
					new PlainTextEncoder());
			final var su = userBuilder.with(username, "duMMy1", "dummy", "dummy", "a@b.ro").build();
			final var us = new UserSession(su);
			return Optional.ofNullable(us);
		};
	}

	protected void assertLastRowIsEmpty(final String result) {
		assertTrue(result.endsWith("\n\n"));
	}

	protected void assertFirstRowIsHeader(final String result, final String[] lines) throws ParseException {
		final var tokens = CsvLineMarshaler.tokenize(lines[0]).toArray(new String[0]);
		assertEquals(6, tokens.length);
		assertEquals("\"ID\"", tokens[0]);
		assertEquals("\"NAME\"", tokens[1]);
		assertEquals("\"TYPE\"", tokens[2]);
		assertEquals("\"CALORIES\"", tokens[3]);
		assertEquals("\"SALT\"", tokens[4]);
		assertEquals("\"PRICE\"", tokens[5]);
	}
}