/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests;

import org.junit.jupiter.api.BeforeEach;

import eapli.ecafeteria.mealbooking.csvprotocol.server.CsvBookingProtocolMessageParser;

/**
 *
 * @author Paulo Gandra Sousa 05/06/2020
 *
 */
class BookMealServerErrorOnThrowsRequestTest extends BaseServerErrorOnThrowsBookingRequestTest {
	private static final String BOOK_MEAL_1_USER1 = "BOOK_A_MEAL, 1, \"user1\", \"pwd\"";

	private CsvBookingProtocolMessageParser parser;

	@BeforeEach
	void setup() {
		parser = new CsvBookingProtocolMessageParser(MockControllers.getMockControllerThrow(), mockAuthenticator());
	}

	@Override
	protected CsvBookingProtocolMessageParser parser() {
		return parser;
	}

	@Override
	protected String getSampleOkRequest() {
		return BOOK_MEAL_1_USER1;
	}
}
