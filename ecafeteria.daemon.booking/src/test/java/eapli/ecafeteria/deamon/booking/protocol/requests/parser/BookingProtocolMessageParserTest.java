/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests.parser;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.mealbooking.csvprotocol.server.BadRequest;
import eapli.ecafeteria.mealbooking.csvprotocol.server.BookingProtocolRequest;
import eapli.ecafeteria.mealbooking.csvprotocol.server.CsvBookingProtocolMessageParser;
import eapli.ecafeteria.mealbooking.csvprotocol.server.UnknownRequest;
import eapli.framework.infrastructure.authz.application.Authenticator;
import eapli.framework.infrastructure.authz.application.UserSession;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;

/**
 * Base class for Protocol parser testing.
 *
 * @author Paulo Gandra Sousa 03/06/2020
 *
 */
public abstract class BookingProtocolMessageParserTest {

	protected abstract CsvBookingProtocolMessageParser parser();

	private static final Logger LOGGER = LogManager.getLogger(BookingProtocolMessageParserTest.class);

	protected Authenticator mockAuthenticator() {
		return (username, rawPassword, requiredRoles) -> {
			final SystemUserBuilder userBuilder = new SystemUserBuilder(new NilPasswordPolicy(),
					new PlainTextEncoder());
			final var su = userBuilder.with(username, "duMMy1", "dummy", "dummy", "a@b.ro").build();
			final var us = new UserSession(su);
			return Optional.ofNullable(us);
		};
	}

	/**
	 * helper method
	 *
	 * @param msg
	 * @param expected
	 * @return
	 */
	protected BookingProtocolRequest ensureParseResultsInMessageOfType(final String msg,
			final Class<? extends BookingProtocolRequest> expected) {
		final var result = parser().parse(msg);
		assertTrue(result.getClass().isAssignableFrom(expected));

		return result;
	}

	/**
	 * helper method
	 *
	 * @param msg
	 * @return
	 */
	protected BookingProtocolRequest ensureErrorInRequest(final String msg) {
		return ensureParseResultsInMessageOfType(msg, BadRequest.class);
	}

	/**
	 * helper method
	 *
	 * @param msg
	 */
	protected void ensureErrorInRequest(final String[] msg) {
		for (var i = 0; i < msg.length; i++) {
			LOGGER.info("Testing for ERROR_IN_REQUEST #{}: {}", i, msg[i]);
			ensureParseResultsInMessageOfType(msg[i], BadRequest.class);
		}
		LOGGER.info("Testing for ERROR_IN_REQUEST - All Clear");
	}

	/**
	 * helper method
	 *
	 * @param msg
	 * @return
	 */
	protected BookingProtocolRequest ensureUnknownRequest(final String msg) {
		return ensureParseResultsInMessageOfType(msg, UnknownRequest.class);
	}

	/**
	 * helper method
	 *
	 * @param msg
	 */
	protected void ensureUnknownRequest(final String[] msg) {
		for (var i = 0; i < msg.length; i++) {
			LOGGER.info("Testing for UNKNOWN_REQUEST #{}: {}", i, msg[i]);
			ensureParseResultsInMessageOfType(msg[i], UnknownRequest.class);
		}
		LOGGER.info("Testing for UNKNOWN_REQUEST - All Clear");
	}
}