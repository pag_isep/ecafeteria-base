/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests.parser;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eapli.ecafeteria.deamon.booking.protocol.requests.MockControllers;
import eapli.ecafeteria.mealbooking.csvprotocol.server.CsvBookingProtocolMessageParser;
import eapli.ecafeteria.mealbooking.csvprotocol.server.GetAvailableMealsRequest;

/**
 * Test plan:
 * <ul>
 * <li>ensure rightly formatted message (for each message type) is parsed
 * correctly</li>
 * <li>ensure wrongly formatted message (for each message type) is parsed to
 * `ERROR_IN_REQUEST`</li>
 * <li>ensure unknown message type is parsed to `UnknownRequest`</li>
 * </ul>
 *
 * @author Paulo Gandra Sousa 03/06/2020
 */
class BookingProtocolMessageParserGetAvailableMealsTest extends BookingProtocolMessageParserTest {
	private CsvBookingProtocolMessageParser parser;

	@BeforeEach
	void setup() {
		parser = new CsvBookingProtocolMessageParser(MockControllers.getMockControllerUnknowns(), mockAuthenticator());
	}

	@Override
	protected CsvBookingProtocolMessageParser parser() {
		return parser;
	}

	// OK
	static final String GET_AVAILABLE_MEALS_OK = "GET_AVAILABLE_MEALS, \"03/06/2020\", \"LUNCH\"";

	private static final String[] SINTAX_ERROR_FIXTURES = {
			// missing quotes
			"GET_AVAILABLE_MEALS,  03/06/2020, \"LUNCH\"",
			// missing quotes
			"GET_AVAILABLE_MEALS,\"03/06/2020\", LUNCH",
			// missing quotes
			"GET_AVAILABLE_MEALS, \"03/06/2020\", LUNCH",
			// missing quotes
			"GET_AVAILABLE_MEALS, \"03/06/2020\", LUNCH",
			// missing param
			"GET_AVAILABLE_MEALS, \"LUNCH\"",
			// missing param
			"GET_AVAILABLE_MEALS, \"03/06/2020\"",
			// missing param
			"GET_AVAILABLE_MEALS,  \"LUNCH\"",
			// extra param
			"GET_AVAILABLE_MEALS,\"03/06/2020\", \"LUNCH\", 123",
			// no content meal type
			"GET_AVAILABLE_MEALS, ,\"03/06/2020\", ",
			// no content date
			"GET_AVAILABLE_MEALS, , , \"LUNCH\"",
			// no content meal type
			"GET_AVAILABLE_MEALS, \"03/06/2020\", ",
			// wrong number of parameters
			"GET_AVAILABLE_MEALS",
			// no content
			"GET_AVAILABLE_MEALS,,,",
			// missing comma
			"GET_AVAILABLE_MEALS  \"03/06/2020\" \"LUNCH\"" };

	@SuppressWarnings("java:S2699")
	@Test
	void ensureParseOfProperMessage() {
		ensureParseResultsInMessageOfType(GET_AVAILABLE_MEALS_OK, GetAvailableMealsRequest.class);
	}

	@SuppressWarnings("java:S2699")
	@Test
	void ensureErrorInRequest() {
		ensureErrorInRequest(SINTAX_ERROR_FIXTURES);
	}
}
