/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eapli.ecafeteria.mealbooking.csvprotocol.server.CsvBookingProtocolMessageParser;

/**
 * @author Paulo Gandra Sousa 03/06/2020
 */
class GetAvailableMealsRequestNoDataTest extends BaseBookingRequestTest {

	private CsvBookingProtocolMessageParser parser;

	@BeforeEach
	void setup() {
		parser = new CsvBookingProtocolMessageParser(MockControllers.getMockControllerNoData(), mockAuthenticator());
	}

	private static final String GET_AVAILABLE_MEALS_05_06_2020_LUNCH = "GET_AVAILABLE_MEALS,  \"05/06/2020\", \"LUNCH\"";

	private static final Logger LOGGER = LogManager.getLogger(GetAvailableMealsRequestNoDataTest.class);

	@Test
	void ensureNoResultsCreatesHeaderOnly() throws ParseException {
		final var req = parser.parse(GET_AVAILABLE_MEALS_05_06_2020_LUNCH);

		final var result = req.execute();
		LOGGER.info("{} -> {}", GET_AVAILABLE_MEALS_05_06_2020_LUNCH, result);

		final var lines = result.split("\n");
		assertEquals(1, lines.length);
		assertFirstRowIsHeader(result, lines);
		assertLastRowIsEmpty(result);
	}
}
