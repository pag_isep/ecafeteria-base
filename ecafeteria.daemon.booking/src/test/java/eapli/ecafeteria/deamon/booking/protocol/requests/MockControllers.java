/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eapli.ecafeteria.cafeteriausermanagement.domain.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.MecanographicNumber;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.domain.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.ecafeteria.mealbooking.application.BookAMealForADayController;
import eapli.ecafeteria.mealbooking.domain.BookingToken;
import eapli.ecafeteria.mealmanagement.domain.Meal;
import eapli.ecafeteria.mealmanagement.domain.MealType;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.money.domain.model.Money;
import eapli.framework.time.util.CurrentTimeCalendars;
import eapli.framework.util.Utility;

/**
 * @author Paulo Gandra de Sousa 09/06/2020
 */
@Utility
public final class MockControllers {

    private MockControllers() {
        // ensure utility
    }

    public static BookAMealForADayController getMockControllerNoData() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                return BookingToken.newToken();
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                return BookingToken.newToken();
            }

            @Override
            public Iterable<Meal> getMealsOfADay(final Calendar when, final MealType forMeal) {
                return new ArrayList<>();
            }
        };
    }

    public static BookAMealForADayController getMockControllerUnknowns() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                throw new IllegalArgumentException("Unknown");
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                throw new IllegalArgumentException("Unknown");
            }

            @Override
            public Iterable<Meal> getMealsOfADay(final Calendar when, final MealType forMeal) {
                throw new IllegalArgumentException("Unknown");
            }
        };
    }

    public static BookAMealForADayController getMockController1RowOfData() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {
            @Override
            public Iterable<Meal> getMealsOfADay(final Calendar when, final MealType forMeal) {
                final var dt = new DishType("fish", "fish");
                final Dish d1 = new DishBuilder().ofType(dt).named(Designation.valueOf("bacalhau"))
                        .costing(Money.euros(7.95)).build();
                final List<Meal> results = new ArrayList<>();

                results.add(new Meal(MealType.LUNCH, CurrentTimeCalendars.now(), d1));
                return results;
            }

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                return BookingToken.newToken();
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                return BookingToken.newToken();
            }

        };
    }

    public static final String NOT_IMPLEMENTED_YET = "not implemented yet";

    public static BookAMealForADayController getMockControllerThrow() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {
            @Override
            public Iterable<Meal> getMealsOfADay(final Calendar when, final MealType forMeal) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }
        };
    }

}