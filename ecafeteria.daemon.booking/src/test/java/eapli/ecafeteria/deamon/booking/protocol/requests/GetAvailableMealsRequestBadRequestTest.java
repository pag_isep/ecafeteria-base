/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import eapli.ecafeteria.mealbooking.csvprotocol.server.CsvBookingProtocolMessageParser;

/**
 * @author Paulo Gandra Sousa 03/06/2020
 */
class GetAvailableMealsRequestBadRequestTest extends BaseBadRequestBookingRequestTest {

	private CsvBookingProtocolMessageParser parser;

	@BeforeEach
	void setup() {
		parser = new CsvBookingProtocolMessageParser(MockControllers.getMockControllerUnknowns(), mockAuthenticator());
	}

	@Override
	protected CsvBookingProtocolMessageParser parser() {
		return parser;
	}

	@ParameterizedTest
	@ValueSource(strings = {
			// empty date
			"GET_AVAILABLE_MEALS,  \"\", \"LUNCH\"",
			// empty meal type
			"GET_AVAILABLE_MEALS,  \"03/06/2020\", \"\"",
			// wrong date
			"GET_AVAILABLE_MEALS,  \"03-06-2020\", \"LUNCH\"",
			// wrong date
			"GET_AVAILABLE_MEALS,  \"03 06/2020\", \"LUNCH\"",
			// wrong date
			"GET_AVAILABLE_MEALS,  \"abc2020\", \"LUNCH\"",
			// wrong meal type
			"GET_AVAILABLE_MEALS,  \"03/06/2020\", \"BREAKFAST\"",
			// wrong meal type
			"GET_AVAILABLE_MEALS,  \"03/06/2020\", \"23ab\"",
			// empty cafeteria meal type
			"GET_AVAILABLE_MEALS,  \"03/06/2020\", \"23ab\"",
			// unknown cafeteria meal type
			"GET_AVAILABLE_MEALS,  \"03/06/2020\", \"23ab\"",
			// invalid cafeteria meal type
			"GET_AVAILABLE_MEALS,  \"03/06/2020\", \"23ab\"", })
	void ensureBadRequest(final String msg) throws ParseException {
		assertTrue(doEnsureBadRequest(msg));
	}
}
