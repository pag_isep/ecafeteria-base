/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * A grammar for dishes for the eCafeteria project. 
 * This simulates the alternate format for dishes with the extension ".dishx"
 *
 * @author Paulo Gandra de Sousa 2024.05.01
 */
 grammar DishAlternate;
 
/*
 * Parser Rules
 */
dishes              
	: dish+ EOF ;

dish				
	: begindish dishbody enddish;
begindish	 		
	: DEF DISH STRING WITH ;
dishbody			
	: typeAttr priceAttr nutritionalAttr? ;
enddish				
	: END DEF;

typeAttr				
	: TYPE '=' WORD ;
priceAttr				
	: PRICE '=' NUMBER CURRENCY ;
nutritionalAttr			
	: saltAttr caloriesAttr ;
saltAttr				
	: SALT '=' NUMBER ; 
caloriesAttr			
	: CALORIES '=' NUMBER ;

/*
 * Lexer Rules
 */

DEF
	: 'def';
DISH
	: 'dish';
WITH				
	: 'with';
END
	: 'end';
TYPE
	: 'type';
PRICE
	: 'price';
SALT
	: 'salt';
CALORIES
	: 'calories' ;
CURRENCY
	: 'eur' 
	| 'usd' 
	| 'EUR' 
	| 'USD' ;

STRING
    : '"' ('""' | ~'"')* '"'
    ; // quote-quote is an escaped quote

fragment DIGIT 		
	: [0-9] ;
NUMBER         		
	: DIGIT+ ([.,] DIGIT+)? ;

fragment LOWERCASE  
	: [a-z] ;
fragment UPPERCASE  
	: [A-Z] ;
WORD                
	: (LOWERCASE | UPPERCASE | '_')+ ;

WS 
	: [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines ;
