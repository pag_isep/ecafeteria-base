/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrations.plugins.dish.alternate;

import java.io.IOException;
import java.io.InputStream;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.ecafeteria.integration.dishes.import_.application.DishImporter;

/**
 * An example plugin for importing dishes.
 * 
 * @author Paulo Gandra de Sousa 2024.04.30
 */
public class AlternateFormatImporter implements DishImporter {

	@Override
	public Iterable<DishDTO> importFrom(final InputStream filename) throws IOException {
		// parse
		final var charStream = CharStreams.fromStream(filename);
		final var lexer = new DishAlternateLexer(charStream);
		final var tokens = new CommonTokenStream(lexer);
		final var parser = new DishAlternateParser(tokens);
		final ParseTree tree = parser.dishes();

		// TODO handle parsing errors

		// traverse the parsing tree
		final var listener = new AlternateDishListener();
		ParseTreeWalker.DEFAULT.walk(listener, tree);

		// return the result of the traversing
		return listener.dishes();
	}

}
