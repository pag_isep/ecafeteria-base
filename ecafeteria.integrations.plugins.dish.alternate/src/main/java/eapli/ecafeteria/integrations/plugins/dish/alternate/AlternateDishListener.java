/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrations.plugins.dish.alternate;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTreeListener;

import eapli.ecafeteria.dishmanagement.dto.DishDTO;

/**
 * 
 * @author Paulo Gandra de Sousa 2024.05.01
 */
public class AlternateDishListener extends DishAlternateBaseListener implements ParseTreeListener {

	private final List<DishDTO> dishes = new ArrayList<>();
	private DishDTO current;

	@Override
	public void enterDish(final DishAlternateParser.DishContext ctx) {
		current = new DishDTO();
	}

	@Override
	public void enterBegindish(final DishAlternateParser.BegindishContext ctx) {
		final var node = ctx.STRING().getText();
		final var name = node.substring(1, node.length() - 1); // unquote the string
		current.setName(name);
	}

	@Override
	public void exitDish(final DishAlternateParser.DishContext ctx) {
		dishes.add(current);
	}

	@Override
	public void enterTypeAttr(final DishAlternateParser.TypeAttrContext ctx) {
		final var node = ctx.WORD().getText();
		current.setDishTypeAcronym(node);
	}

	@Override
	public void enterPriceAttr(final DishAlternateParser.PriceAttrContext ctx) {
		final var node1 = ctx.NUMBER().getText();
		final var price = Double.parseDouble(node1);
		current.setPrice(price);

		final var node2 = ctx.CURRENCY().getText();
		final var currency = node2.toUpperCase();
		current.setCurrency(currency);
	}

	@Override
	public void enterSaltAttr(final DishAlternateParser.SaltAttrContext ctx) {
		final var node = ctx.NUMBER().getText();
		final var salt = Integer.parseInt(node);
		current.setSalt(salt);
	}

	@Override
	public void enterCaloriesAttr(final DishAlternateParser.CaloriesAttrContext ctx) {
		final var node = ctx.NUMBER().getText();
		final var calories = Integer.parseInt(node);
		current.setCalories(calories);
	}

	public List<DishDTO> dishes() {
		return dishes;
	}
}
