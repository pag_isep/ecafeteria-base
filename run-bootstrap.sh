#!/usr/bin/env bash

# set the class path,
# assumes the build was executed with maven copy-dependencies
# we also add the plugin jars to the classpath so they can be found in runtime
export ECAFETERIA_CP=ecafeteria.app.bootstrap/target/app.bootstrap-4.0.0.jar:ecafeteria.app.bootstrap/target/dependency/*;ecafeteria.integrations.plugins.standard\target\integrations.plugins.standard-4.0.0.jar

#REM call the java VM, e.g,
java -cp $ECAFETERIA_CP eapli.ecafeteria.app.bootstrap.ECafeteriaBootstrap -bootstrap:demo -smoke:basic -smoke:e2e
