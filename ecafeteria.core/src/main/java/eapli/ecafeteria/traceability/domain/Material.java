/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.traceability.domain;

import java.util.UUID;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Version;

/**
 * A material (e.g., rice) used in the cooking of a meal.
 */
@Entity
public class Material implements AggregateRoot<String> {

    private static final long serialVersionUID = 1L;

    /*
     * <p>
     * Showcase a custom generator with a prefix. This is just a showcase as in a real scenario it
     * would be advisable for the database's primary key to be a Long.
     */
    /*
     * @GenericGenerator(name = "custom_material", strategy =
     * "eapli.ecafeteria.persistence.impl.jpa.MaterialSequenceGenerator")
     *
     * @GeneratedValue(generator = "custom_material")
     */

    /**
     * ORM primary key.
     */
    @Id
    private final String pk = UUID.randomUUID().toString();

    @Version
    private Long version;

    /**
     * business ID
     */
    @Column(unique = true, nullable = false)
    private final String acronym;

    private String description;

    protected Material() {
        // for ORM
        acronym = null;
    }

    public Material(final String name, final String description) {
        Preconditions.nonEmpty(name);
        this.acronym = name;
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public void changeDescriptionTo(final String newDescription) {
        this.description = newDescription;
    }

    @Override
    public String identity() {
        return this.acronym;
    }

    @Override
    public boolean hasIdentity(final String id) {
        return id.equalsIgnoreCase(this.acronym);
    }

    @Override
    public boolean sameAs(final Object o) {
        if (!this.equals(o)) {
            return false;
        }
        final Material other = (Material) o;
        return acronym.equals(other.acronym) && description.equals(other.description);
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }
}
