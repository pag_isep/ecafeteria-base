/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import jakarta.persistence.Embeddable;
import jakarta.xml.bind.annotation.XmlElement;
import lombok.Value;
import lombok.experimental.Accessors;

/**
 * The nutricional information of a dish.
 * <p>
 * Both {@code calories} and {@code salt} would be a very interesting situation
 * to use <a href="http://martinfowler.com/eaaDev/quantity.html">Quantity
 * pattern</a>
 * </p>
 * <p>
 * Also check the <a hef=
 * "http://jscience.org/api/javax/measure/package-summary.html">javax.measure</a>
 * javadocs or <a href="https://www.baeldung.com/javax-measure">intro</a>
 * </p>
 *
 * @author Jorge Santos ajs@isep.ipp.pt 11/04/2016
 */
@Embeddable
@Value
@Accessors(fluent = true)
public class NutricionalInfo implements ValueObject {

	private static final long serialVersionUID = 1L;

	/**
	 * Special case value of unknown nutricional info. Even tough
	 * {@link Dish#nutricionalInfo()} returns an Optional it might be helpful to
	 * have a "Null value".
	 */
	public static final NutricionalInfo UNKNOWN = new NutricionalInfo();

	@XmlElement
	@JsonProperty
	private final Integer calories;

	@XmlElement
	@JsonProperty
	private final Integer salt;

	/**
	 * @param calories
	 * @param salt
	 */
	public NutricionalInfo(final int calories, final int salt) {
		Preconditions.ensure(calories >= 0, "Calories can't be negative");
		Preconditions.ensure(salt >= 0, "Salt can't be negative");

		this.calories = calories;
		this.salt = salt;
	}

	/**
	 * Special case constructor for unknown values and ORM.
	 */
	protected NutricionalInfo() {
		calories = salt = -1;
	}

	@Override
	public String toString() {
		return this.calories + " " + this.salt;
	}
}
