/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.domain;

import java.util.Set;

import eapli.framework.domain.model.DomainFactory;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.money.domain.model.Money;

/**
 * A Dish builder. Since there are two optional components of Dish, i.e.,
 * nutricional info and allergens, we are using a builder to simplify the class
 * and avoid overloading constructors or too much conditional logic on the
 * constructor.
 *
 * @author Paulo Gandra de Sousa 2021/05/04
 */
public class DishBuilder implements DomainFactory<Dish> {

    private Dish theDish;

    private Designation name;
    private DishType type;
    private Money price;

    public DishBuilder ofType(final DishType dishType) {
        type = dishType;
        return this;
    }

    public DishBuilder named(final String name) {
        return named(Designation.valueOf(name));
    }

    public DishBuilder named(final Designation name) {
        this.name = name;
        return this;
    }

    public DishBuilder costing(final Money price) {
        this.price = price;
        return this;
    }

    private Dish buildOrThrow() {
        if (theDish != null) {
            return theDish;
        }
        if (name != null && type != null && price != null) {
            theDish = new Dish(type, name, price);
            return theDish;
        } else {
            throw new IllegalStateException();
        }
    }

    public DishBuilder withNutricionalInfo(final NutricionalInfo nut) {
        buildOrThrow();
        theDish.changeNutricionalInfoTo(nut);
        return this;
    }

    public DishBuilder withAllergens(final Set<Allergen> allergens) {
        // we will simply ignore if we receive a null set
        if (allergens != null) {
            allergens.forEach(this::withAllergen);
        }
        return this;
    }

    public DishBuilder withAllergen(final Allergen allergen) {
        buildOrThrow();
        theDish.addAllergen(allergen);
        return this;
    }

    @Override
    public Dish build() {
        final Dish ret = buildOrThrow();
        // make sure we will create a new instance if someone reuses this builder and do
        // not change
        // the previously built dish.
        theDish = null;
        return ret;
    }
}
