/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integration.dishes.import_.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.dishmanagement.application.viadto.DishDTOParser;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.ecafeteria.dishmanagement.repositories.DishRepository;
import eapli.ecafeteria.dishmanagement.repositories.DishTypeRepository;
import eapli.ecafeteria.infrastructure.persistence.PersistenceContext;
import eapli.ecafeteria.integration.dishes.import_.domain.FileExtension;
import eapli.ecafeteria.integration.dishes.import_.repositories.DishImporterPluginRepository;
import eapli.ecafeteria.usermanagement.domain.CafeteriaRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 * @author Paulo Gandra de Sousa 2024.04.30
 */
@UseCaseController
public class ImportDishesController {
	private static final Logger LOGGER = LogManager.getLogger(ImportDishesController.class);

	private final AuthorizationService authz = AuthzRegistry.authorizationService();
	private final DishImporterPluginRepository pluginRepo = PersistenceContext.repositories().dishImporterPlugins();
	private final DishRepository dishRepository = PersistenceContext.repositories().dishes();
	private final DishTypeRepository dishTypeRepository = PersistenceContext.repositories().dishTypes();

	/**
	 * Import dishes from a file. It uses the file extension to determine which
	 * import plugin to activate.
	 * <p>
	 * If there is an error parsing the file no dish will be imported.
	 * 
	 * @param filename
	 * @return the list of imported dishes
	 * @throws IOException
	 */
	public List<Dish> importDishes(final String filename) throws IOException {
		authz.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);

		// TODO refactor this method to move logic from the controller into a service
		// class

		// prepare the result variable
		List<Dish> dishes = new ArrayList<>();

		// get the content of the file to import
		InputStream content = null;
		try {
			// get the content of the file to import
			content = inputStreamFromResourceOrFile(filename);

			// get the right plugin for the file
			final var fileExt = FilenameUtils.getExtension(filename);
			final var plugin = pluginRepo.findByFileExtension(FileExtension.valueOf(fileExt)).orElseThrow(
					() -> new IllegalStateException("There is no plugin associated with that file extension"));

			// load the plugin
			final var importer = plugin.buildImporter();

			// parse the content
			final var dishesToRegister = importer.importFrom(content);

			// do the import
			dishes = doTheImport(dishesToRegister);
		} finally {
			if (content != null) {
				try {
					content.close();
				} catch (final IOException e) {
					LOGGER.error("Error closing the file {}", filename);
				}
			}
		}

		return dishes;
	}

	private List<Dish> doTheImport(final Iterable<DishDTO> dishesToRegister) {
		// TODO begin transaction
		final List<Dish> dishes = new ArrayList<>();
		for (final var dto : dishesToRegister) {
			// TODO handle errors, like unknown dish type or unknown currency,
			// IllegalArgumentException
			final var newDish = new DishDTOParser(dishTypeRepository).valueOf(dto);
			// TODO check what should be done if we are trying to import a dish that already
			// exists
			final var savedDish = dishRepository.save(newDish);
			dishes.add(savedDish);
		}
		// TODO commit transaction
		return dishes;
	}

	private InputStream inputStreamFromResourceOrFile(final String filename) throws FileNotFoundException {
		InputStream content;
		final var classLoader = this.getClass().getClassLoader();
		final var resource = classLoader.getResource(filename);
		if (resource != null) {
			final var file = new File(resource.getFile());
			content = new FileInputStream(file);
		} else {
			content = new FileInputStream(filename);
		}
		return content;
	}
}
