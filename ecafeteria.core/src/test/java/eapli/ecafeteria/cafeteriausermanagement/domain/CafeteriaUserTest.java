/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import eapli.ecafeteria.cafeteriausermanagement.util.CafeteriaUserTestUtil;

/**
 * @author Nuno Bettencourt [NMB] on 03/04/16.
 */
class CafeteriaUserTest {

	private final String aMecanographicNumber = "abc";
	private final String anotherMecanographicNumber = "xyz";

	@Test
	void ensureCafeteriaUserEqualsPassesForTheSameMecanographicNumber() throws Exception {

		final var aCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser();

		final var anotherCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser();

		final var expected = aCafeteriaUser.equals(anotherCafeteriaUser);

		assertTrue(expected);
	}

	@Test
	void ensureCafeteriaUserEqualsFailsForDifferenteMecanographicNumber() throws Exception {
		final var aCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser(aMecanographicNumber);

		final var anotherCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser(anotherMecanographicNumber);

		final var expected = aCafeteriaUser.equals(anotherCafeteriaUser);

		assertFalse(expected);
	}

	@Test
	void ensureCafeteriaUserEqualsAreTheSameForTheSameInstance() throws Exception {
		final var aCafeteriaUser = new CafeteriaUser();

		assertEquals(aCafeteriaUser, aCafeteriaUser);
	}

	@Test
	void ensureCafeteriaUserEqualsFailsForDifferenteObjectTypes() throws Exception {
		final var aCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser();

		@SuppressWarnings("unlikely-arg-type")
		final var expected = aCafeteriaUser.equals(CafeteriaUserTestUtil.getNewDummyUser());

		assertFalse(expected);
	}

	@Test
	void ensureCafeteriaUserIsTheSameAsItsInstance() throws Exception {
		final var aCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser();

		assertTrue(aCafeteriaUser.sameAs(aCafeteriaUser));
	}

	@Test
	void ensureTwoCafeteriaUserWithDifferentMecanographicNumbersAreNotTheSame() throws Exception {
		final var aCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser(aMecanographicNumber);

		final var anotherCafeteriaUser = CafeteriaUserTestUtil.getDummyCafeteriaUser(anotherMecanographicNumber);

		assertFalse(aCafeteriaUser.sameAs(anotherCafeteriaUser));
	}
}
