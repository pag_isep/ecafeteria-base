/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealmanagement.domain;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Calendar;

import org.junit.jupiter.api.Test;

import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.domain.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.money.domain.model.Money;
import eapli.framework.time.util.Calendars;

/**
 * @author Paulo Gandra de Sousa 12/03/2024
 *
 */
class MealTest {
	// dish
	private static final Designation BACALHAU_NAME = Designation.valueOf("Bacalhau");
	private static final DishType FISH_DISH_TYPE = new DishType("fish", "fish");

	// other
	private final Calendar aDay = Calendars.of(2024, 3, 12);
	private final Dish aDish = new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME).costing(Money.euros(7))
			.build();

	@Test
	void givenEverythingIsOkThenAMealIsCreated() {
		final var subject = new Meal(MealType.DINNER, aDay, aDish);
		assertNotNull(subject);
	}

	@Test
	void ensureMustHaveMealType() {
		assertThrows(IllegalArgumentException.class, () -> new Meal(null, aDay, aDish));
	}

	@Test
	void ensureMustHaveDate() {
		assertThrows(IllegalArgumentException.class, () -> new Meal(MealType.DINNER, null, aDish));
	}

	@Test
	void ensureMustHaveDish() {
		assertThrows(IllegalArgumentException.class, () -> new Meal(MealType.DINNER, aDay, null));
	}
}
