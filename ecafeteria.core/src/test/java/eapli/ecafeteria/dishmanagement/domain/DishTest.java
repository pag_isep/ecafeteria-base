/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import eapli.framework.general.domain.model.Designation;
import eapli.framework.money.domain.model.Money;

/**
 * @author Paulo Gandra Sousa
 */
class DishTest {

    private static final Designation BACALHAU_NAME = Designation.valueOf("Bacalhau");
    private static final DishType FISH_DISH_TYPE = new DishType("fish", "fish");

    private Dish buildDish() {
        return new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME).costing(Money.euros(7))
                .withNutricionalInfo(new NutricionalInfo(1, 1)).build();
    }

    @Test
    void ensureDishWithDishTypeDesignationAndPrice() {
        new Dish(FISH_DISH_TYPE, BACALHAU_NAME, Money.euros(1.0));
        assertTrue(true);
    }

    @Test
    void ensureMustHaveDishType() {
        final var price = Money.euros(1.0);
        assertThrows(IllegalArgumentException.class, () -> new Dish(null, BACALHAU_NAME, price));
    }

    @Test
    void ensureMustHaveDesignation() {
        final var price = Money.euros(1.0);
        assertThrows(IllegalArgumentException.class, () -> new Dish(FISH_DISH_TYPE, null, price));
    }

    @Test
    void ensureMustHavePrice() {
        assertThrows(IllegalArgumentException.class, () -> new Dish(FISH_DISH_TYPE, BACALHAU_NAME, null));
    }

    /**
     * Test of changeNutricionalInfoTo method, of class Dish. PRP - 29.mar.2017
     */
    @Test
    void ensureCannotChangeNutricionalInfoToNull() {
        System.out.println("ChangeNutricionalInfoTo -New nutricional info must not be null");

        final Dish subject = buildDish();

        assertThrows(IllegalArgumentException.class, () -> subject.changeNutricionalInfoTo(null));
    }

    @Test
    void ensureCanChangeNutricionalInfo() {
        // ARRANJE
        final Dish subject = buildDish();
        final NutricionalInfo newInfo = new NutricionalInfo(100, 100);

        // ACT
        subject.changeNutricionalInfoTo(newInfo);

        // ASSERT
        assertEquals(newInfo, subject.nutricionalInfo().get());
    }

    /**
     * Tests of changePriceTo method, of class Dish. PRP - 29.mar.2017
     */
    @Test
    void ensureCannotChangePriceToNull() {
        System.out.println("ChangePriceTo - New price info must not be null");

        final Dish subject = buildDish();

        assertThrows(IllegalArgumentException.class, () -> subject.changePriceTo(null));
    }

    @Test
    void ensureCanChangePrice() {
        final Dish subject = buildDish();
        final Money newPrice = Money.euros(5000);
        subject.changePriceTo(newPrice);

        assertEquals(newPrice, subject.currentPrice());
    }

    @Test
    void ensureCannotChangePriceToNegative() {
        System.out.println("ChangePriceTo - New price can't be negative");

        final Dish subject = buildDish();
        final var price = Money.euros(-1);
        assertThrows(IllegalArgumentException.class, () -> subject.changePriceTo(price));
    }

}
