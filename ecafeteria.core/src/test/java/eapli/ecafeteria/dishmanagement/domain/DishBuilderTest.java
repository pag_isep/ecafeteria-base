/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.domain;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.Test;

import eapli.framework.general.domain.model.Designation;
import eapli.framework.money.domain.model.Money;

/**
 * @author Paulo Gandra Sousa
 */
public class DishBuilderTest {

    private static final Designation BACALHAU_NAME = Designation.valueOf("Bacalhau");
    private static final DishType FISH_DISH_TYPE = new DishType("fish", "fish");

    private Dish buildDish() {
        return new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME).costing(Money.euros(7))
                .withNutricionalInfo(new NutricionalInfo(1, 1)).build();
    }

    @Test
    public void ensureCanBuildDishWithDishTypeDesignationPriceAndNutricionalInfo() {
        final Dish subject = new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME).costing(Money.euros(7))
                .build();
        assertNotNull(subject);
    }

    @Test
    public void ensureCanBuildWithNullNutricionalInfo() {
        final Dish subject = buildDish();
        assertNotNull(subject);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotBuildWithNullNutricionalInfo() {
        new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME).costing(Money.euros(7)).withNutricionalInfo(null)
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void ensureCannotBuildWithNullDishType() {
        new DishBuilder().ofType(null).named(BACALHAU_NAME).costing(Money.euros(7))
                .withNutricionalInfo(new NutricionalInfo(1, 1)).build();
    }

    @Test(expected = IllegalStateException.class)
    public void ensureCannotBuildWithNullDishType2() {
        new DishBuilder().named(BACALHAU_NAME).costing(Money.euros(7)).withNutricionalInfo(new NutricionalInfo(1, 1))
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotBuildWithNullName() {
        new DishBuilder().ofType(FISH_DISH_TYPE).named((String) null).costing(Money.euros(7))
                .withNutricionalInfo(new NutricionalInfo(1, 1)).build();
    }

    @Test(expected = IllegalStateException.class)
    public void ensureCannotBuildWithNullName2() {
        new DishBuilder().ofType(FISH_DISH_TYPE).costing(Money.euros(7)).withNutricionalInfo(new NutricionalInfo(1, 1))
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void ensureCannotBuildWithNullPrice() {
        new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME).costing(null)
                .withNutricionalInfo(new NutricionalInfo(1, 1)).build();
    }

    @Test(expected = IllegalStateException.class)
    public void ensureCannotBuildWithNullPrice2() {
        new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME).withNutricionalInfo(new NutricionalInfo(1, 1))
                .build();
    }
}
