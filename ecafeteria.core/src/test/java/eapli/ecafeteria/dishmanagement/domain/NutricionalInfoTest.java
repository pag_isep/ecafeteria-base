/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Test;

/**
 * @author AntónioRocha
 */
public class NutricionalInfoTest {

	private static final Integer SALT = 5;
	private static final Integer CALORIES = 3;

	@Test
	public void ensureCaloriesMustNotBeNegative() {
		System.out.println("calories must not be negative");

		assertThrows(IllegalArgumentException.class, () -> new NutricionalInfo(-1, 0));
	}

	@Test
	public void ensureSaltMustNotBeNegative() {
		System.out.println("Salt must not be negative");

		assertThrows(IllegalArgumentException.class, () -> new NutricionalInfo(0, -6));
	}

	@Test
	public void ensureCaloriesHasRightValue() {
		System.out.println("calories");
		final var instance = new NutricionalInfo(CALORIES, 5);
		assertEquals(CALORIES, instance.calories());
	}

	@Test
	public void ensureSaltHasRightValue() {
		System.out.println("calories");
		final var instance = new NutricionalInfo(3, SALT);
		assertEquals(SALT, instance.salt());
	}
}
