/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.application.viadto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.domain.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.ecafeteria.dishmanagement.domain.NutricionalInfo;
import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.money.domain.model.Money;

/**
 * @author Paulo Gandra de Sousa
 */
class DishDTORepresentationalBuilderTest {

	private static final Designation BACALHAU_NAME = Designation.valueOf("Bacalhau");
	private static final DishType FISH_DISH_TYPE = new DishType("fish", "fish");
	private final Dish SIMPLE_BIZ_OBJ = new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME)
			.costing(Money.euros(1.0)).build();
	private final Dish WITH_NUTR_BIZ_OBJ = new DishBuilder().ofType(FISH_DISH_TYPE).named(BACALHAU_NAME)
			.costing(Money.euros(1.0)).withNutricionalInfo(new NutricionalInfo(1, 1)).build();

	@Test
	void ensureBuildReturnsAnObject() {
		final var subject = SIMPLE_BIZ_OBJ.buildRepresentation(new DishDTORepresentationBuilder());

		assertNotNull(subject);
	}

	@Test
	void ensureBuildSimpleDish() {
		final var subject = SIMPLE_BIZ_OBJ.buildRepresentation(new DishDTORepresentationBuilder());

		assertEquals(SIMPLE_BIZ_OBJ.isActive(), subject.isActive());
		assertEquals(SIMPLE_BIZ_OBJ.name().toString(), subject.getName());
		assertEquals(SIMPLE_BIZ_OBJ.dishType().identity().toString(), subject.getDishTypeAcronym());
		assertEquals(SIMPLE_BIZ_OBJ.dishType().description().toString(), subject.getDishTypeDescription());
		assertEquals(SIMPLE_BIZ_OBJ.currentPrice().amountAsDouble(), subject.getPrice(), 0.001);
		assertEquals(SIMPLE_BIZ_OBJ.currentPrice().currency().toString(), subject.getCurrency());
		assertEquals(DishDTO.NO_INFO, subject.getCalories());
		assertEquals(DishDTO.NO_INFO, subject.getSalt());
	}

	@Test
	void ensureBuildDishWithNutricionalInfo() {
		final var subject = WITH_NUTR_BIZ_OBJ.buildRepresentation(new DishDTORepresentationBuilder());

		assertEquals(WITH_NUTR_BIZ_OBJ.isActive(), subject.isActive());
		assertEquals(WITH_NUTR_BIZ_OBJ.name().toString(), subject.getName());
		assertEquals(WITH_NUTR_BIZ_OBJ.dishType().identity().toString(), subject.getDishTypeAcronym());
		assertEquals(WITH_NUTR_BIZ_OBJ.dishType().description().toString(), subject.getDishTypeDescription());
		assertEquals(WITH_NUTR_BIZ_OBJ.currentPrice().amountAsDouble(), subject.getPrice(), 0.001);
		assertEquals(WITH_NUTR_BIZ_OBJ.currentPrice().currency().toString(), subject.getCurrency());
		assertEquals(WITH_NUTR_BIZ_OBJ.nutricionalInfo().get().calories(), subject.getCalories());
		assertEquals(WITH_NUTR_BIZ_OBJ.nutricionalInfo().get().salt(), subject.getSalt());
	}
}
