/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.application;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.ecafeteria.dishmanagement.repositories.DishTypeRepository;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;

/**
 * expect:
 * <ul>
 * <li>- validate user permission: menu manager, power user
 * <li>- constructs the domain object with the right values
 * <li>- call repository save
 * <li>- do not silence exceptions from other components:
 * <li>- if domain (dishtype) throws exception, rethrow
 * <li>- if authz throws exception, rethrow
 * <li>- if repo throws exception, rethrow
 * </ul>
 * 
 * @author Paulo Gandra Sousa 2023/02/20
 */
class RegisterDishTypeControllerTest {

	public static SystemUser dummyUser(final String username, final Role... roles) {
		final var userBuilder = new SystemUserBuilder(new NilPasswordPolicy(), new PlainTextEncoder());
		return userBuilder.with(username, "duMMy1", "dummy", "dummy", "a@b.ro").withRoles(roles).build();
	}

	// mock objects
	private final AuthorizationService unauthorizedAuthz = new AuthorizationService() {
		@Override
		public void ensureAuthenticatedUserHasAnyOf(final Role... actions) {
			throw new UnauthorizedException(dummyUser("X"), actions);
		}
	};
	private final AuthorizationService authorizedAuthz = new AuthorizationService() {
		@Override
		public void ensureAuthenticatedUserHasAnyOf(final Role... actions) {
		}
	};
	private final DishTypeRepository repoDuplicateOnSave = new DishTypeRepository() {

		@Override
		public <S extends DishType> S save(final S entity) {
			throw new IntegrityViolationException("");
		}

		@Override
		public Iterable<DishType> findAll() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Optional<DishType> ofIdentity(final String id) {
			// TODO Auto-generated method stub
			return Optional.empty();
		}

		@Override
		public void delete(final DishType entity) {
			// TODO Auto-generated method stub

		}

		@Override
		public void deleteOfIdentity(final String entityId) {
			// TODO Auto-generated method stub

		}

		@Override
		public long count() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public DishType lockItUp(final DishType entity) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Optional<DishType> lockOfIdentity(final String id) {
			// TODO Auto-generated method stub
			return Optional.empty();
		}

		@Override
		public Iterable<DishType> activeDishTypes() {
			// TODO Auto-generated method stub
			return null;
		}

	};

	private final DishTypeRepository repoSave = new DishTypeRepository() {

		@Override
		public <S extends DishType> S save(final S entity) {
			return entity;
		}

		@Override
		public Iterable<DishType> findAll() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Optional<DishType> ofIdentity(final String id) {
			// TODO Auto-generated method stub
			return Optional.empty();
		}

		@Override
		public void delete(final DishType entity) {
			// TODO Auto-generated method stub

		}

		@Override
		public void deleteOfIdentity(final String entityId) {
			// TODO Auto-generated method stub

		}

		@Override
		public long count() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public DishType lockItUp(final DishType entity) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Optional<DishType> lockOfIdentity(final String id) {
			// TODO Auto-generated method stub
			return Optional.empty();
		}

		@Override
		public Iterable<DishType> activeDishTypes() {
			// TODO Auto-generated method stub
			return null;
		}

	};

	@Test
	void whenDishTypeAlreadyExistsThenAnExceptionIsThrown() {
		final var subject = new RegisterDishTypeController(authorizedAuthz, repoDuplicateOnSave);

		assertThrows(IntegrityViolationException.class, () -> subject.registerDishType("veg", "veggie"));
	}

	@Test
	void whenUserIsNotMenuManagerThenAnExceptionIsThrown() {
		final var subject = new RegisterDishTypeController(unauthorizedAuthz, null);

		assertThrows(UnauthorizedException.class, () -> subject.registerDishType("veg", "veggie"));
	}

	@Test
	void whenUserIsMenuManagerAndDishTypeDoesntYetExistThenDishTypeIsCreated() {
		final var subject = new RegisterDishTypeController(authorizedAuthz, repoSave);
		final var createdDish = subject.registerDishType("veg", "veggie");
		assertNotNull(createdDish);
	}
}
