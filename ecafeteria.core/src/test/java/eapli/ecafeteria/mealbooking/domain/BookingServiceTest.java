/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.mealbooking.domain;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import eapli.ecafeteria.cafeteriausermanagement.domain.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.util.CafeteriaUserTestUtil;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.domain.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.ecafeteria.mealmanagement.domain.Meal;
import eapli.ecafeteria.mealmanagement.domain.MealType;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.money.domain.model.Money;
import eapli.framework.time.util.CurrentTimeCalendars;

/**
 * @author Paulo Gandra Sousa 2024.03.12
 */
class BookingServiceTest {

	private final DishType aDishType = new DishType("fish", "fish");
	private final Money aPrice = Money.euros(3);
	private final Dish aDish = new DishBuilder().ofType(aDishType).named(Designation.valueOf("Braised Cod"))
			.costing(aPrice).build();
	private final Meal tomorrowsMeal = new Meal(MealType.LUNCH, CurrentTimeCalendars.tomorrow(), aDish);
	private final Meal yesterdayMeal = new Meal(MealType.LUNCH, CurrentTimeCalendars.yesterday(), aDish);
	private final CafeteriaUser aUser = CafeteriaUserTestUtil.getDummyCafeteriaUser();
	private final Money enoughBalance = aPrice.multiply(3);

	BookingService subject = new BookingService();

	@Test
	void givenIHaveEnoughBalanceWhenIBookAMealThenIHaveABooking() {
		// GIVEN
		final var balance = enoughBalance;
		// WHEN
		final var booking = subject.bookMeal(tomorrowsMeal, aUser, balance);
		// THEN
		assertNotNull(booking);
	}

	@Test
	void givenIDontHaveEnoughBalanceWhenIBookAMealThenIDontGetABooking() {
		// GIVEN
		final var balance = Money.euros(1);
		// WHEN-THEN
		assertThrows(IllegalStateException.class, () -> subject.bookMeal(tomorrowsMeal, aUser, balance));
	}

	@Test
	void givenIHaveEnoughBalanceButIWantToBookAMealInThePastThenICantBookAMeal() {
		// GIVEN
		final var balance = enoughBalance;
		// THEN
		assertThrows(IllegalStateException.class, () -> subject.bookMeal(yesterdayMeal, aUser, balance));

	}

	@Test
	void ensureBalanceMustNotBeNull() {
		// ARRANGE
		// ACT-ASSERT
		assertThrows(IllegalArgumentException.class, () -> subject.bookMeal(tomorrowsMeal, aUser, null));
	}

	@Test
	void ensureCafeteriaUserMustNotBeNull() {
		// ARRANGE
		// ACT-ASSERT
		assertThrows(IllegalArgumentException.class, () -> subject.bookMeal(tomorrowsMeal, null, enoughBalance));
	}

	@Test
	void ensureMealMustNotBeNull() {
		// ARRANGE
		// ACT-ASSERT
		assertThrows(IllegalArgumentException.class, () -> subject.bookMeal(null, aUser, enoughBalance));
	}
}
