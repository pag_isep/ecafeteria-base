/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrations.plugins.dish.standard;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTreeListener;

import eapli.ecafeteria.dishmanagement.dto.DishDTO;

/**
 * 
 * @author Paulo Gandra de Sousa 2024.05.01
 */
public class ImportListener extends FoodDescriptionBaseListener implements ParseTreeListener {

	private final List<DishDTO> dishes = new ArrayList<>();
	private DishDTO current;

	@Override
	public void enterFoodEntry(final FoodDescriptionParser.FoodEntryContext ctx) {
		current = new DishDTO();
	}

	@Override
	public void enterFoodName(final FoodDescriptionParser.FoodNameContext ctx) {
		final var node = ctx.STRING().getText();
		final var name = node.substring(1, node.length() - 1); // unquote the string
		current.setName(name);

	}

	@Override
	public void exitFoodEntry(final FoodDescriptionParser.FoodEntryContext ctx) {
		dishes.add(current);
	}

	@Override
	public void enterFoodType(final FoodDescriptionParser.FoodTypeContext ctx) {
		final var node = ctx.WORD().getText();
		current.setDishTypeAcronym(node);
	}

	@Override
	public void enterCostDescription(final FoodDescriptionParser.CostDescriptionContext ctx) {

		final var node1 = ctx.NUMBER().getText();
		final var price = Double.parseDouble(node1);
		current.setPrice(price);

		// the grammar only allows eur
		final var node2 = "eur";// ctx.CURRENCY().getText();
		final var currency = node2.toUpperCase();
		current.setCurrency(currency);
	}

	@Override
	public void enterSalt(final FoodDescriptionParser.SaltContext ctx) {
		final var node = ctx.NUMBER().getText();
		final var salt = Integer.parseInt(node);
		current.setSalt(salt);
	}

	@Override
	public void enterCalories(final FoodDescriptionParser.CaloriesContext ctx) {
		final var node = ctx.NUMBER().getText();
		final var calories = Integer.parseInt(node);
		current.setCalories(calories);
	}

	public List<DishDTO> dishes() {
		return dishes;
	}
}
