/*
 * Generated with ChatGPT by providing the following prompt:
 *
 *
 * 		please create an ANTLR grammar for the following program:
 * 
 * 		"bacalhau à lagareiro" of type peixe costs 10 eur, and has 3 gram of salt and 490 calories
 * 		"ostras ao natural" of type peixe costs 12.9 eur
 * 		"ovos mexidos" of type carne costs 3.75 eur, and has 1 gram of salt and 180 calories
 * 
 * and then some manual tweaks of the generated grammar
 */
grammar FoodDescription;

// Parser rules
program: foodEntry (NEWLINE foodEntry)* NEWLINE?;

foodEntry: foodName 'of type' foodType costDescription (',' nutrientDescription)?;

foodName: STRING;

foodType: WORD;

costDescription: 'costs' NUMBER 'eur';

nutrientDescription: 'and has' salt 'and' calories;

salt: NUMBER 'gram' 'of' 'salt';

calories: NUMBER 'calories';

// Lexer rules
STRING
    : '"' ('""' | ~'"')* '"'
    ; // quote-quote is an escaped quote
fragment LOWERCASE  
	: [a-z] ;
fragment UPPERCASE  
	: [A-Z] ;
WORD                
	: (LOWERCASE | UPPERCASE | '_')+ ;
NUMBER: ('0'..'9')+ ('.' ('0'..'9')+)?;
NEWLINE: '\r'? '\n';
WS: [ \t]+ -> skip;