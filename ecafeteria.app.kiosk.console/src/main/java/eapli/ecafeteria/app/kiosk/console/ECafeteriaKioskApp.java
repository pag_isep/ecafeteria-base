/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.kiosk.console;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.cafeteria.app.common.console.ECafeteriaBaseApplication;
import eapli.cafeteria.app.common.console.presentation.authz.LoginUI;
import eapli.ecafeteria.app.kiosk.authz.CredentialStore;
import eapli.ecafeteria.app.kiosk.console.presentation.BookAMealThruKioskUI;
import eapli.ecafeteria.mealbooking.application.KioskBookAMealController;
import eapli.ecafeteria.mealbooking.csvprotocol.client.FailedRequestException;
import eapli.framework.infrastructure.pubsub.EventDispatcher;
import eapli.framework.io.util.Console;
import eapli.framework.time.util.CurrentTimeCalendars;

/**
 * eCafeteria KIOSK Application. The client of the Booking Daemon.
 *
 * @author Paulo Gandra de Sousa 2021.05.25
 */
@SuppressWarnings("squid:S106")
public final class ECafeteriaKioskApp extends ECafeteriaBaseApplication {
	private static final Logger LOGGER = LogManager.getLogger(ECafeteriaKioskApp.class);

	/**
	 * Empty constructor is private to avoid instantiation of this class.
	 */
	private ECafeteriaKioskApp() {
	}

	@Override
	protected void configureAuthz() {
		// override base configuration
	}

	public static void main(final String[] args) {

		new ECafeteriaKioskApp().run(args);
	}

	@Override
	protected void doMain(final String[] args) {

		smokeTestBookingServer();

		// we will simulate a simple Kiosk which allows the user to enter her card and
		// book a meal
		// for a certain day.
		var loop = true;
		do {
			System.out.println("\n»»»» Simulating »»»» card inserted in the card reader...");
			final var correctPin = new LoginUI(CredentialStore.STORE_CREDENTIALS).show();
			if (correctPin) {
				new BookAMealThruKioskUI().show();
			} else {
				System.out.println("»»»» Simulating »»»» catching the card of the user...");
			}
			System.out.println("»»»» Simulating »»»» big on/off button on the kiosk...");
			loop = !Console.readBoolean("Turn off? (y/n)");
		} while (loop);

	}

	private void smokeTestBookingServer() {

		try {
			// simulate login
			CredentialStore.STORE_CREDENTIALS.authenticated("user1", "Password1", null);

			final var theController = new KioskBookAMealController();
			// to fully execute this smoke test, make sure to load data for meals lunch
			// time "tomorrow"
			final var meals = theController.getAvailableMeals(CurrentTimeCalendars.tomorrow(), "LUNCH");
			if (meals.iterator().hasNext()) {
				final var token = theController.bookMealForMe(CredentialStore.getUsername(),
						CredentialStore.getPassword(), meals.iterator().next());
				System.out.println("Booked " + token);
			} else {
				System.out.println("There are no available meals to book at the desired date and cafeteria.");
			}
		} catch (final IOException e) {
			System.out.println("Problems with network connection: " + e.getMessage());
			LOGGER.debug(e);
		} catch (final FailedRequestException e) {
			System.out.println("Problems with request: " + e.getMessage());
		}
	}

	@Override
	protected String appTitle() {
		return "KIOSK eCafeteria";
	}

	@Override
	protected String appGoodbye() {
		return "Thank you for using 'KIOSK eCafeteria'";
	}

	@Override
	protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
		// nothing to do
	}
}
